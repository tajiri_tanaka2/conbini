class UsersController < ApplicationController
  def index
    @stores = Store.all
    @users = User.all
  end
  
  def new
    @users = User.new
    @stores = Store.new
  end
  
  def create
    @user = User.new(name: params[:user][:name], pass: params[:user][:pass])
    if @user.save
      flash[:info] = '登録しました。'
      redirect_to users_login_path 
    else
      render new_user_path
    end
  end
  
  def destroy
    reset_session
    redirect_to root_path
  end
  
  def login
      @user = User.find_by(name: params[:name],pass: params[:pass])
     if @user
       session[:login_uid] = @user.id
       flash[:info] = 'ログインしています。'
       redirect_to users_path 
     else
        flash[:miss] = 'もう一度入力してください。'
      render users_login_path
      
     end
  end    

  def show
   session.delete(:login_uid)
   flash[:info] = 'ログアウトしました。'
   redirect_to users_login_path 
  end
end