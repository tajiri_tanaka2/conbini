class TopController < ApplicationController
  def index
      if session[:login_uid]
         render 'main'
      else
         render 'login'
      end
  end
end
