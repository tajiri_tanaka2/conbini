class StoresController < ApplicationController
  def index
    @stores = Store.all
  end
  
  def new
    @stores = Store.new
  end
  
  def create
    @store = Store.new(name: params[:store][:name], pass: params[:store][:pass], tel: params[:store][:tel], period_product: params[:store][:period_product], store_product: params[:store][:store_product], pay: params[:store][:pay], card: params[:store][:card], atm: params[:store][:atm], other: params[:store][:other])
    if @store.save
      flash[:info] = 'ユーザを登録完了しました。'
      redirect_to stores_login_path
    else
      render new_store_path
    end
  end  
  
  def destroy
    reset_session
    redirect_to root_path, notice: 'ログアウトしました。'
  end
  
  def login
    unless params[:name] && params[:pass]
      render 'login'
    else
      if Store.find_by(name: params[:name], pass: params[:pass])
        session[:login_store] = params[:name]
        pp "T"
        redirect_to stores_path
      else 
        flash[:warning] = '名前、もしくはパスワードが間違っています。'
        pp "F"
        render 'login'
      end
    end
  end
  
  def show
    @store = Store.find(params[:id])
  end
  
  def edit
    @store = Store.find(params[:id])
  end

  def update
    @store = Store.find(params[:id])
    @store.update(name: params[:store][:name],tel: params[:store][:tel], 期間限定の商品: params[:store][:period_product], 当店限定の商品: params[:store][:store_product], pay: params[:store][:pay], card: params[:store][:card], atm: params[:store][:atm], その他: params[:store][:other])
    redirect_to '/'
  end
  
end