class CreateCusts < ActiveRecord::Migration[5.2]
  def change
    create_table :custs, :id => false do |t|
      t.string :name
      t.integer :id
      t.string :pass

      t.timestamps
    end
  end
end
