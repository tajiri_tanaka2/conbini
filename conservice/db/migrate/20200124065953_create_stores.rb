class CreateStores < ActiveRecord::Migration[5.2]
  def change
    create_table :stores do |t|
      t.string :name
      t.string :pass
      t.string :tel
      t.string :period_product
      t.string :store_product
      t.string :pay
      t.string :card
      t.string :atm
      t.string :other

      t.timestamps
    end
  end
end
