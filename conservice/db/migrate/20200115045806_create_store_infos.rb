class CreateStoreInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :store_infos do |t|
      t.string :sid
      t.string :tel
      t.string :name
      t.string :period_product
      t.string :store_product
      t.string :pay
      t.string :card
      t.string :atm
      t.string :other

      t.timestamps
    end
  end
end
