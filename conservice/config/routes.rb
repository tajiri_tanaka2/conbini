Rails.application.routes.draw do


  root 'top#login'

  get 'top/main'
  post 'users/login'
  post 'users/logout'
  post 'stores/login'
  post 'stores/logout'
  
  get 'stores/login'
  post 'stores/login'

  get 'users/login'
  post 'users/login'
  
  get 'stores/edit'
  post 'stores/edit'
  

  
  resources :stores
  resources :users
  resources :top
end